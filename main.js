const sr = ScrollReveal()
sr.reveal('.maintenance', {
    duration: 2000,
    reset: true,
    origin: 'bottom',
    mobile: true
})

sr.reveal('.lilargan', {
    duration: 2000,
    reset: true,
    origin: 'top',
    mobile: true
})